'use strict';

// Declare app level module which depends on views, and components
var myApp = angular.module('myApp', [
    'ngRoute',
    'myApp.login',
    'myApp.version',
    'myApp.createAccount',
    'myApp.accountCreated',
    'myApp.wall'
]);

myApp.config(['$locationProvider', '$routeProvider', '$httpProvider', '$resourceProvider', function($locationProvider, $routeProvider, $httpProvider, $resourceProvider) {
    $locationProvider.hashPrefix('!');
    $routeProvider.otherwise({redirectTo: '/wall'});

    $httpProvider.interceptors.push(function ($q, $rootScope, $location) {
        return {
            'request': function (config) {
                config.headers.Authorization = $rootScope.token ? "JWT "+ $rootScope.token : "JWT " +localStorage.token;
                return config;
            },
            'responseError': function(rejection) {
                console.log("rejection", rejection);
                // do something on error
                if(rejection.statu=401)
                    $location.path("/login");

                return $q.reject(rejection);
            },
            'response': function (response) {
                return response;
            }
        }
    });

    //$resourceProvider.defaults.stripTrailingSlashes = false;

}]);

myApp.value('apiUrl', 'http://localhost:3000');


myApp.factory('posts',["$resource", "apiUrl", function ($resource, apiUrl) {
    return $resource(apiUrl + '/posts/:id', {}, {'update':{method:'PUT'}, get:{method:'GET'}});
}]);

myApp.factory('comments', ["$resource", "apiUrl", function ($resource, apiUrl) {
    return $resource(apiUrl + '/comments/:id',{}, {update:{method:'PUT'}, get:{method:'GET', isArray:true}});
}]);

myApp.factory('likePost', ["$resource", "apiUrl", function ($resource, apiUrl) {
    return $resource(apiUrl + '/posts/:id/like', {}, {like:{method:'get'}})
}]);

myApp.factory('likeComment', ["$resource", "apiUrl", function ($resource, apiUrl) {
    return $resource(apiUrl + '/comments/:id/like', {}, {like:{method:'get'}})
}]);

myApp.factory("token", ["$resource", "apiUrl", function ($resource, apiUrl) {
    return $resource(apiUrl + '/token')
}]);

myApp.factory("user", ["$resource", "apiUrl", function ($resource, apiUrl) {
    return $resource(apiUrl + '/user', {save: {method:'POST'}})
}]);

myApp.controller("LogOutCtrl",['$location', '$rootScope', "$scope", function ($location, $rootScope, $scope) {
    $scope.logOut=  function () {
            $rootScope.token = null;
            localStorage.token = null;
            $location.path('/login');
        }
}]);

