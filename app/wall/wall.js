/**
 * Created by marvi on 15/06/2016.
 */
/**
 * Created by marvi on 14/06/2016.
 */
'use strict';

angular.module('myApp.wall', ['ngRoute', 'ngResource', 'myApp'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/wall', {
            templateUrl: 'wall/wall.html',
            controller: 'WallCtrl',
            replace: true
        });
    }])

    .controller('WallCtrl', ['likeComment', 'likePost', '$rootScope', "$scope", "posts", 'comments', "$location", function( likeComment, likePost, $rootScope, $scope, posts, comments, $location) {

        $scope.createPost = function (post) {
            var post_class = new posts(post);

            $scope.post.error = null;

            post_class.$save(

                function (data) {
                    console.log("created" + data);
                    $scope.getPosts($scope.currentPage)
                },

                function (errorData) {
                    $scope.post.error = errorData.data;
                }
            );
        };

        $scope.goToCreateAccount = function () {
            $location.path('/createAccount')
        };

        $scope.showNewPostModal = function () {
            console.log("clicked")
            $("#PostModal").openModal();
        };

        $scope.openCommentModal = function (index) {
            var post = $scope.posts[index];

            $scope.getComments(post.id, function (comments) {
                $scope.comments = comments;
            });

            $scope.currentPostOpened = post.id;

            $('#commentsModal').openModal();
        };

        $scope.showCreateCommentModal = function () {
            //$('#commentsModal').closeModal();
            $("#NewCommentModal").openModal();
        };

        $scope.createComment = function () {

            $scope.comment.post =  $scope.currentPostOpened;

            var comment_class = new comments($scope.comment);

            $scope.comment.error = null;

            comment_class.$save(
                function () {
                    $scope.getComments($scope.currentPostOpened)
                },
                function (errorData) {
                    $scope.comment.error = errorData.data;
                }
            )
        };

        $scope.getPost = function (postId) {
            posts.get({id: postId}, function (data) {
                $scope.posts[postId] = data;
            },
            function (errorData) {

            })
        };
        $scope.likeComment = function (index, commentID) {
            likeComment.like({id: commentID}, function (data) {
                $scope.comments[index] = data;
            }, function (errorData) {
                console.log(errorData)
            });
        };

        $scope.likePost = function (index, postId) {
            console.log("linking");
            likePost.like({id: postId}, function (data) {
                $scope.getPosts($scope.currentPage)
            }, function (errorData) {
                console.log(errorData)
            })
        };

        $scope.getComments = function (postId, callback) {
            comments.get({post:postId},{},
                function (data) {
                    $scope.comments = data;
                    if(callback)
                        callback($scope.comments);
                },
                function (errorData) {
                    if(callback)
                        callback(null, errorData);

                    console.log("error at getComments", errorData)
                }
            )
        };

        $scope.getAuthorName = function () {

        };

        $scope.currentPage = 1;

        $scope.getPosts = function (page) {

            posts.get({page:page ? page: 1, page_size:4},
                function (data) {
                    $scope.currentPage = page || 1;
                    $scope.postPages = Math.ceil(data.count / 4);
                    $scope.postPageNumbers = [];
                    for(var x = 1;x<$scope.postPages+1;x++){
                        $scope.postPageNumbers.push(x);
                    }
                    $scope.posts = data.results;
                    $scope.posts.map(function (currentPost) {
                        var counter = 0;
                        currentPost.contentSplited = currentPost.content.split('\n').map(function (content) {
                            return {id:counter++, text:content}
                        });
                        return currentPost
                    })
                },
                function (errorData) {
                    console.log("error at getPosts", errorData);
                    console.log(localStorage.getItem('token'));
                });
        };
        $scope.getPosts()

    }]);