/**
 * Created by marvi on 15/06/2016.
 */
angular.module('myApp.accountCreated', ['ngRoute', 'ngResource', 'myApp'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/accountCreated', {
            template: '<h4 class="truncate">Account Created Successfully click <a href="/#login">here</a> to login</h4>',
            controller: function () {
                
            },
            replace: true
        });
    }]);