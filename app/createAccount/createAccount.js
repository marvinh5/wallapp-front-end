/**
 * Created by marvi on 15/06/2016.
 */
/**
 * Created by marvi on 14/06/2016.
 */
'use strict';

angular.module('myApp.createAccount', ['ngRoute', 'ngResource', 'myApp'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/createAccount', {
            templateUrl: 'createAccount/createAccount.html',
            controller: 'CreateAccountCtrl',
            replace: true
        });
    }])

    .controller('CreateAccountCtrl', ['$rootScope', "$scope", "user", "$location", function($rootScope, $scope, user, $location) {

        $scope.createAccount = function () {
            var user_class = new user(
                {
                    username: $scope.username,
                    password:$scope.password,
                    first_name:$scope.first_name,
                    last_name:$scope.last_name,
                    email: $scope.email
                });

            $scope.error = null;

            user_class.$save(

                function (data) {

                    console.log('logged data', data, 'scope data', $scope);

                    $location.path("/accountCreated");

                },

                function (errorData) {

                    $scope.error = errorData.data;
                }
            );
        }

    }]);