/**
 * Created by marvi on 14/06/2016.
 */
'use strict';

angular.module('myApp.login', ['ngRoute', 'ngResource', 'myApp'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/login', {
        templateUrl: 'login/login.html',
        controller: 'LoginCtrl',
        replace: true
    });
}])

.controller('LoginCtrl', ['$rootScope', "$scope", "token", "$location", function($rootScope, $scope, token, $location) {

    $scope.login = function () {
        var token_class = new token({username: $scope.username, password:$scope.password});

        $scope.error = null;

        token_class.$save(

            function (data) {

                $rootScope.token = data.token;

                localStorage.token = data.token;

                $location.path("/wall");

            },

            function (errorData) {

                $scope.error = errorData.data;
            }
        );
    };

    $scope.goToCreateAccount = function () {
        $location.path('/createAccount')
    }

}]);